<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 11.05.2023
 */

$xxx_restsmart_default_option = [
    'debug' => 'Y',
    'debug_chat_id' => 'chat123456'
];