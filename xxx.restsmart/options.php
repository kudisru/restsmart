<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) { die(); }

/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 11.05.2023
 */

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Rest\APAuth\PasswordTable;
use Xxx\Core\Crm\Smart;
use Xxx\Core\Options\Render;
use Xxx\RestSmart\General\Instruments;

try {
    if (
        !Loader::includeModule('xxx.restsmart') ||
        !Loader::includeModule('xxx.core') ||
        !Loader::includeModule('rest') ||
        !Loader::includeModule('crm')
    ) {
        return false;
    }
} catch (LoaderException $e) {
    return false;
}

$sModuleId = Instruments::MODULE_ID;

$arAllSmartTypes = Smart::getTypes();
$arSmartTypes = array_column($arAllSmartTypes, 'TITLE', 'ENTITY_TYPE_ID');

$obRestApps = PasswordTable::getList(
    [
        'filter' => [
            'ACTIVE' => 'Y'
        ],
        'select' => [
            'ID',
            'TITLE'
        ],
        'order' => [
            'ID' => 'desc'
        ]
    ]
);
$arApps = [];
while ($arApp = $obRestApps->fetch()) {
    $arApps[$arApp['ID']] = $arApp['TITLE'];
}

$arTabs = [];
$arTabs['main'] = [
    'TAB' => [
        'DIV' => 'main',
        'TAB' => Loc::getMessage('XXX_REST_SMART__MAIN_TAB'),
        'TITLE' => Loc::getMessage('XXX_REST_SMART__MAIN_TAB_DESCRIPTION')
    ],
    'VALUES' => []
];

foreach ($arSmartTypes as $iSmartId => $sSmartName) {
    $sValueKey = $iSmartId . '_smart_title';
    $arTabs['main']['VALUES'][$sValueKey] = [
        'option' => false,
        'input' => Loc::getMessage('XXX_REST_SMART__ACCESS_FOR_APPS', ['#NAME#' => $sSmartName])
    ];
    $sValueKey = 'smart_apps_' . $iSmartId;
    $arTabs['main']['VALUES'][$sValueKey] = [
        'option' => false,
        'input' => [
            $sValueKey,
            Loc::getMessage('XXX_REST_SMART__APPS_LIST'),
            '',
            [
                'multiselectbox',
                $arApps
            ]
        ]
    ];
}

$arTabs['debug'] = [
    'TAB' => [
        'DIV' => 'debug',
        'TAB' => Loc::getMessage('XXX_REST_SMART__DEBUG_TAB'),
        'TITLE' => Loc::getMessage('XXX_REST_SMART__DEBUG_TAB_DESCRIPTION')
    ],
    'VALUES' => []
];

$sValueKey = 'debug';
$arTabs['debug']['VALUES'][$sValueKey] = [
    'option' => 'N',
    'input' => [
        $sValueKey,
        Loc::getMessage('XXX_REST_SMART__DEBUG'),
        0,
        ['checkbox']
    ]
];

$sValueKey = 'debug_chat_id';
$arTabs['debug']['VALUES'][$sValueKey] = [
    'option' => '',
    'input'  => [
        $sValueKey,
        Loc::getMessage('XXX_REST_SMART__DEBUG_CHAT_ID'),
        '',
        ['text', 20]
    ]
];

$obOptionsRender = new Render($sModuleId, $arTabs);
$obOptionsRender->show();