<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 16.02.2023
 */

$MESS['XXX_REST_SMART__MAIN_TAB'] = 'Главная';
$MESS['XXX_REST_SMART__MAIN_TAB_DESCRIPTION'] = 'Основные настройки';
$MESS['XXX_REST_SMART__ACCESS_FOR_APPS'] = 'Доступность процесса `#NAME#` для Веб-хуков';
$MESS['XXX_REST_SMART__APPS_LIST'] = 'Веб-хуки';
$MESS['XXX_REST_SMART__DEBUG_TAB'] = 'Отладка';
$MESS['XXX_REST_SMART__DEBUG_TAB_DESCRIPTION'] = 'Настройки режима отладки';
$MESS['XXX_REST_SMART__DEBUG'] = 'Дебаг режим';
$MESS['XXX_REST_SMART__DEBUG_CHAT_ID'] = 'Чат для отладочных сообщений';