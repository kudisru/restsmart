<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 11.05.2023
 */

/** @var array $MESS */

$MESS['XXX_REST_SMART__WRONG_SCOPE'] = 'The request requires higher privileges than provided by the webhook token';
$MESS['XXX_REST_SMART__EMPTY_TYPE_ID'] = 'Empty `options`.`TYPE_ID`';
$MESS['XXX_REST_SMART__WRONG_TYPE_ID'] = 'Не найден Смарт процесс с `options`.`TYPE_ID` = #TYPE_ID#';
$MESS['XXX_REST_SMART__EMPTY_ID_FIELD'] = 'Не установлен или не верный `fields`.`#ID_FIELD_NAME#`, указанный в `options`.`ID_FIELD_NAME`';
$MESS['XXX_REST_SMART__WRONG_ID_FIELD'] = 'Не найдено поле `options`.`ID_FIELD_NAME` = #FIELD_NAME#';
$MESS['XXX_REST_SMART__UNKNOWN_FIELD'] = 'Неизвестное поле `fields`.`#FIELD_NAME#`';
$MESS['XXX_REST_SMART__WRONG_CURRENCY_FORMAT'] = 'Не верный формат поля `fields`.`#FIELD_NAME#` (Пример: 12.34|RUB)';
$MESS['XXX_REST_SMART__WRONG_CURRENCY'] = 'Не верная валюта в значении поля `fields`.`#FIELD_NAME#` = \'#FIELD_VALUE#\' (Доступны: #AVAILABLE_CURRENCIES#)';
$MESS['XXX_REST_SMART__CANT_FIND_ELEMENT'] = 'Не найден элемент Смарт процесса с `#FIELD_NAME#` = \'#FIELD_VALUE#\'';