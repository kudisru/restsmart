<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 11.05.2023
 */

$MESS['XXX_REST_SMART__MODULE_NAME'] = 'XXX REST Smart';
$MESS['XXX_REST_SMART__MODULE_DESCRIPTION'] = 'Эндпойнт для создания и изменения элементов смарт процесса';
$MESS['XXX_REST_SMART__INSTALL_TITLE'] = 'Module \'REST Smart\' successfully installed';
$MESS['XXX_REST_SMART__UNINSTALL_TITLE'] = 'Module \'REST Smart\' successfully uninstalled';