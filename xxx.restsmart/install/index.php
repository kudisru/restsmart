<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}

/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 11.05.2023
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;

if (class_exists('xxx_restsmart')) {
    return;
}

class xxx_restsmart extends CModule
{
    private $sModuleInstallPath = '';

    public function __construct()
    {
        $arModuleVersion = [];
        $this->MODULE_ID = 'xxx.restsmart';
        $this->MODULE_GROUP_RIGHTS = 'Y';
        $path = realpath(__FILE__);
        $this->sModuleInstallPath = substr($path, 0, strlen($path) - strlen('/index.php'));
        include_once($this->sModuleInstallPath . '/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('XXX_REST_SMART__MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('XXX_REST_SMART__MODULE_DESCRIPTION');
        $this->PARTNER_NAME = 'Sergei Kudisov';
        $this->PARTNER_URI = 'programming@kudis.ru';
    }

    public function doInstall()
    {
        global $USER, $APPLICATION;
        if ($USER->IsAdmin() && !IsModuleInstalled($this->MODULE_ID)) {
            ModuleManager::registerModule($this->MODULE_ID);
            $this->installEvents();
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage('XXX_REST_SMART__INSTALL_TITLE'),
                $this->sModuleInstallPath . '/step1.php'
            );
        }
    }

    public function doUninstall()
    {
        global $USER, $APPLICATION;
        if ($USER->IsAdmin() && IsModuleInstalled($this->MODULE_ID)) {
            $this->unInstallEvents();
            ModuleManager::unRegisterModule($this->MODULE_ID);
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage('XXX_REST_SMART__UNINSTALL_TITLE'),
                $this->sModuleInstallPath . '/unstep1.php'
            );
        }
    }

    private function getEvents() : array
    {
        return [
            [
                'rest',
                'OnRestServiceBuildDescription',
                $this->MODULE_ID,
                '\Xxx\RestSmart\Rest',
                'onRestServiceBuildDescription'
            ]
        ];
    }

    public function installEvents()
    {
        $eventManager = EventManager::getInstance();
        foreach ($this->getEvents() as $arArguments) {
            $eventManager->registerEventHandlerCompatible(...$arArguments);
        }
    }

    public function unInstallEvents()
    {
        $eventManager = EventManager::getInstance();
        foreach ($this->getEvents() as $arArguments) {
            $eventManager->unregisterEventHandler(...$arArguments);
        }
    }

}
