<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 11.05.2023
 */

$arModuleVersion = [
    'VERSION'      => '1.0.0',
    'VERSION_DATE' => '2023-05-11 13:31:00'
];