<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) { die(); }

/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 23.06.2022
 */

/** @global CMain $APPLICATION */
if (!check_bitrix_sessid()) {
    return;
}

if ($ex = $APPLICATION->GetException()) {
    $obMessage = new CAdminMessage(
        [
            'TYPE'    => 'ERROR',
            'MESSAGE' => GetMessage('MOD_UNINST_ERR'),
            'DETAILS' => $ex->GetString(),
            'HTML'    => true
        ]
    );
} else {
    $obMessage = new CAdminMessage(
        [
            'TYPE'    => 'OK',
            'MESSAGE' => GetMessage('MOD_UNINST_OK')
        ]
    );
}
echo $obMessage->Show();
?>
<form action="<?= $APPLICATION->GetCurPage(); ?>">
    <input type="hidden" name="lang" value="<?= LANG ?>">
    <input type="submit" name="" value="<?= GetMessage("MOD_BACK"); ?>">
<form>