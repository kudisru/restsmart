<?php
/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 11.05.2023
 */

namespace Xxx\RestSmart\General;

use CRestServer;
use CIMMessenger;
use Bitrix\Main\Loader;
use Bitrix\Main\UserTable;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Config\Option;
use Bitrix\Rest\RestException;
use Xxx\Core\Crm\Smart;
use Xxx\Core\Tools\UserFields;

class Instruments
{

    public const MODULE_ID = 'xxx.restsmart';

    const STATUS_BAD_REQUEST = CRestServer::STATUS_WRONG_REQUEST;
    const STATUS_FORBIDDEN = CRestServer::STATUS_FORBIDDEN;
    const STATUS_NOT_FOUND = CRestServer::STATUS_NOT_FOUND;
    const STATUS_CONFLICT = '409 Conflict';

    public const AVAILABLE_CURRENCIES = [
        'RUB',
        'EUR',
        'USD'
    ];

    protected int $iSmartProcessId = 0;
    protected string $sElementId = '';
    protected string $sIdFieldName = 'ID';

    protected array $arErrors = [];
    protected string $sErrorStatus = '';

    protected array $arFields = [];

    public function __construct()
    {
        Loader::includeModule('crm');
        Loader::includeModule('xxx.core');
    }

    public function checkScope($obRestServer): bool
    {
        $bResult = false;

        if (
            $obRestServer instanceof CRestServer &&
            ($arQuery = $obRestServer->getQuery()) &&
            isset($arQuery['options']['TYPE_ID']) &&
            ($iSmartId = (int) $arQuery['options']['TYPE_ID']) &&
            $iSmartId > 0 &&
            $obRestServer->getPasswordId() > 0 &&
            (
                $arAppsWithAccess = array_filter(
                    array_map(
                        'trim',
                        explode(
                            ',',
                            Option::get(self::MODULE_ID, 'smart_apps_' . $iSmartId, '')
                        )
                    )
                )
            ) &&
            !empty($arAppsWithAccess) &&
            in_array($obRestServer->getPasswordId(), $arAppsWithAccess)
        ) {
            $this->iSmartProcessId = $iSmartId;
            $bResult = true;
        }

        if (!$bResult) {
            $this->arErrors[] = Loc::getMessage('XXX_REST_SMART__WRONG_SCOPE');
            $this->sErrorStatus = self::STATUS_FORBIDDEN;
        }

        return $bResult;
    }

    public function prepareFields(array $arRequestData): array
    {
        $arResult = [];

        if (
            !isset($arRequestData['options']['TYPE_ID']) ||
            $arRequestData['options']['TYPE_ID'] <= 0
        ) {
            $this->arErrors[] = Loc::getMessage('XXX_REST_SMART__EMPTY_TYPE_ID');
            $this->sErrorStatus = self::STATUS_BAD_REQUEST;
            return $arResult;
        }

        $arSmartTypes = Smart::getTypes();
        if (!in_array($arRequestData['options']['TYPE_ID'], array_column($arSmartTypes, 'ENTITY_TYPE_ID'))) {
            $this->arErrors[] = Loc::getMessage(
                'XXX_REST_SMART__WRONG_TYPE_ID',
                ['#TYPE_ID#' => $arRequestData['options']['TYPE_ID']]
            );
            $this->sErrorStatus = self::STATUS_BAD_REQUEST;
            return $arResult;
        } else {
            if (
                isset($arRequestData['options']['ID_FIELD_NAME']) &&
                !empty($arRequestData['options']['ID_FIELD_NAME']) &&
                $arRequestData['options']['ID_FIELD_NAME'] !== 'ID'
            ) {
                if (!isset($this->arFields[$arRequestData['options']['ID_FIELD_NAME']])) {
                    $arFieldData = UserFields::getFieldDataCached(
                        [
                            'ENTITY_ID' => 'CRM_' .
                                array_column(
                                    $arSmartTypes,
                                    'ID',
                                    'ENTITY_TYPE_ID'
                                )
                                [$arRequestData['options']['TYPE_ID']],
                            'NAME'      => $arRequestData['options']['ID_FIELD_NAME']
                        ]
                    );
                    if (!empty($arFieldData)) {
                        $this->arFields[$arRequestData['options']['ID_FIELD_NAME']] = $arFieldData;
                    }
                }
                if (empty($this->arFields[$arRequestData['options']['ID_FIELD_NAME']])) {
                    $this->arErrors[] = Loc::getMessage(
                        'XXX_REST_SMART__WRONG_ID_FIELD',
                        ['#FIELD_NAME#' => $arRequestData['options']['ID_FIELD_NAME']]
                    );
                    $this->sErrorStatus = self::STATUS_BAD_REQUEST;
                    return $arResult;
                }
                if (empty($arRequestData['fields'][$arRequestData['options']['ID_FIELD_NAME']])) {
                    $this->arErrors[] = Loc::getMessage(
                        'XXX_REST_SMART__EMPTY_ID_FIELD',
                        ['#ID_FIELD_NAME#' => $arRequestData['options']['ID_FIELD_NAME']]
                    );
                    $this->sErrorStatus = self::STATUS_BAD_REQUEST;
                    return $arResult;
                }
                $this->sElementId = $arRequestData['fields'][$arRequestData['options']['ID_FIELD_NAME']];
                $this->sIdFieldName = $arRequestData['options']['ID_FIELD_NAME'];
            } elseif (!empty($arRequestData['fields'][$this->sIdFieldName])) {
                $this->sElementId = (string) $arRequestData['fields'][$this->sIdFieldName];
            }
        }

        foreach ($arRequestData['fields'] as $sFieldName => $mxValue) {
            if (preg_match('/^UF_.+$/', $sFieldName) === 1) {
                if (empty($this->arFields[$sFieldName])) {
                    $arFieldData = UserFields::getFieldDataCached(
                        [
                            'ENTITY_ID'    => 'CRM_' .
                                array_column(
                                    $arSmartTypes,
                                    'ID',
                                    'ENTITY_TYPE_ID'
                                )
                                [$arRequestData['options']['TYPE_ID']],
                            'NAME'         => $sFieldName,
                            'USER_TYPE_ID' => ''
                        ]
                    );
                    if (!empty($arFieldData)) {
                        $this->arFields[$sFieldName] = $arFieldData;
                    }
                }
                if (
                    !empty($this->arFields[$sFieldName]) &&
                    $this->arFields[$sFieldName]['ID'] > 0
                ) {
                    if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                        if (is_array($mxValue)) {
                            $arValue = array_values($mxValue);
                        } else {
                            $arValue = [$mxValue];
                        }
                    } else {
                        if (is_array($mxValue)) {
                            $arValue = [array_values($mxValue)[0]];
                        } else {
                            $arValue = [$mxValue];
                        }
                    }
                    foreach ($arValue as $mxItemValue) {
                        switch ($this->arFields[$sFieldName]['USER_TYPE_ID']) {
                            case 'employee':
                                if ((int) $mxItemValue > 0) {
                                    if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                        $arResult[$sFieldName][] = (int) $mxItemValue;
                                    } else {
                                        $arResult[$sFieldName] = (int) $mxItemValue;
                                    }
                                } else {
                                    $obUser = UserTable::getList(
                                        [
                                            'select' => [
                                                'ID'
                                            ],
                                            'filter' => [
                                                [
                                                    'LOGIC' => 'OR',
                                                    [
                                                        '=LOGIN' => mb_strtolower($mxItemValue)
                                                    ],
                                                    [
                                                        '=LOGIN' => str_replace(
                                                            '@3l.ru',
                                                            '',
                                                            mb_strtolower($mxItemValue)
                                                        )
                                                    ]
                                                ]
                                            ],
                                            'cache'  => ['ttl' => 86400],
                                            'limit'  => 1
                                        ]
                                    );
                                    if ($arUser = $obUser->fetch()) {
                                        if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                            $arResult[$sFieldName][] = $arUser['ID'];
                                        } else {
                                            $arResult[$sFieldName] = $arUser['ID'];
                                        }
                                    }
                                }
                                break;
                            case 'money':
                                if (preg_match(
                                        '/^(\d+(\.\d+)?)\|(.+)$/',
                                        str_replace(',', '.', $mxItemValue),
                                        $arMatches
                                    ) === 1) {
                                    if (in_array($arMatches[3], self::AVAILABLE_CURRENCIES)) {
                                        if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                            $arResult[$sFieldName][] = $mxItemValue;
                                        } else {
                                            $arResult[$sFieldName] = $mxItemValue;
                                        }
                                    } else {
                                        $this->arErrors[] = Loc::getMessage(
                                            'XXX_REST_SMART__WRONG_CURRENCY',
                                            [
                                                '#FIELD_NAME#'           => $sFieldName,
                                                '#FIELD_VALUE#'          => $mxItemValue,
                                                '#AVAILABLE_CURRENCIES#' => implode(', ', self::AVAILABLE_CURRENCIES)
                                            ]
                                        );
                                        $this->sErrorStatus = self::STATUS_BAD_REQUEST;
                                    }
                                } else {
                                    $this->arErrors[] = Loc::getMessage(
                                        'XXX_REST_SMART__WRONG_CURRENCY_FORMAT',
                                        [
                                            '#FIELD_NAME#' => $sFieldName
                                        ]
                                    );
                                    $this->sErrorStatus = self::STATUS_BAD_REQUEST;
                                }
                                break;
                            case 'enumeration':
                                if (isset($this->arFields[$sFieldName]['ENUMS'][$mxItemValue])) {
                                    if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                        $arResult[$sFieldName][] =
                                            $this->arFields[$sFieldName]['ENUMS'][$mxItemValue]['ID'];
                                    } else {
                                        $arResult[$sFieldName] =
                                            $this->arFields[$sFieldName]['ENUMS'][$mxItemValue]['ID'];
                                    }
                                } elseif (
                                    ($iValue = array_column(
                                                   $this->arFields[$sFieldName]['ENUMS'],
                                                   'ID',
                                                   'XML_ID'
                                               )[$mxItemValue]) &&
                                    $iValue > 0
                                ) {
                                    if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                        $arResult[$sFieldName][] = $iValue;
                                    } else {
                                        $arResult[$sFieldName] = $iValue;
                                    }
                                } elseif (
                                    ($iValue = array_column(
                                                   $this->arFields[$sFieldName]['ENUMS'],
                                                   'ID',
                                                   'NAME'
                                               )[$mxItemValue]) &&
                                    $iValue > 0
                                ) {
                                    if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                        $arResult[$sFieldName][] = $iValue;
                                    } else {
                                        $arResult[$sFieldName] = $iValue;
                                    }
                                }
                                break;
                            case 'boolean':
                                if ($mxItemValue !== true && $mxItemValue !== false) {
                                    $mxItemValue = in_array(mb_strtolower($mxItemValue), ['true', 'y']);
                                }
                                if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                    $arResult[$sFieldName][] = $mxItemValue;
                                } else {
                                    $arResult[$sFieldName] = $mxItemValue;
                                }
                                break;
                            case 'date':
                            case 'datetime':
                                $obDate = DateTime::createFromTimestamp(strtotime($mxItemValue));
                                if ($obDate->getDiff(new DateTime())->y < 30) {
                                    if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                        $arResult[$sFieldName][] = $obDate;
                                    } else {
                                        $arResult[$sFieldName] = $obDate;
                                    }
                                }
                                break;
                            default:
                                if ($this->arFields[$sFieldName]['MULTIPLE'] === 'Y') {
                                    $arResult[$sFieldName][] = $mxItemValue;
                                } else {
                                    $arResult[$sFieldName] = $mxItemValue;
                                }
                        }
                    }
                } else {
                    $this->arErrors[] = Loc::getMessage(
                        'XXX_REST_SMART__UNKNOWN_FIELD',
                        ['#FIELD_NAME#' => $sFieldName]
                    );
                    $this->sErrorStatus = self::STATUS_BAD_REQUEST;
                }
            } else {
                $arResult[$sFieldName] = $mxValue;
            }
        }

        return $arResult;
    }

    public function update(array $arFields, $bCreate = false): int
    {
        $iElementId = 0;

        $obSmart = new Smart(
            $this->iSmartProcessId,
            [
                'CHECK_PERMISSIONS' => 'N',
                'CHECK_FIELDS' => 'N'
            ]
        );

        if (!empty($this->sElementId) && !empty($this->sIdFieldName)) {
            $arExitingSmarts = $obSmart->getItems(
                [
                    'filter' => [
                        '=' . $this->sIdFieldName => $this->sElementId
                    ],
                    'select' => [
                        'ID'
                    ],
                    'limit' => 1
                ]
            );
            if (
                !empty($arExitingSmarts) &&
                isset($arExitingSmarts[0]['ID'])
            ) {
                $iElementId = (int) $arExitingSmarts[0]['ID'];
            }
        }

        if ($iElementId > 0) {
            unset($arFields[$this->sIdFieldName]);
            $obResult = $obSmart->update(
                $iElementId,
                $arFields
            );
            if (!$obResult->isSuccess()) {
                $this->arErrors = array_merge($this->arErrors, $obResult->getErrors());
                $this->sErrorStatus = self::STATUS_FORBIDDEN;
            }
        } elseif ($bCreate) {
            if ($this->sIdFieldName === 'ID') {
                unset($arFields[$this->sIdFieldName]);
            }
            $obResult = $obSmart->add(
                $arFields
            );
            if ($obResult->isSuccess()) {
                $iElementId = (int) $obResult->getData()['ID'];
            } else {
                $this->arErrors = array_merge($this->arErrors, $obResult->getErrors());
                $this->sErrorStatus = self::STATUS_FORBIDDEN;
            }
        } else {
            $this->arErrors[] = Loc::getMessage(
                'XXX_REST_SMART__CANT_FIND_ELEMENT',
                [
                    '#FIELD_NAME#' => $this->sIdFieldName,
                    '#FIELD_VALUE#' => $this->sElementId
                ]
            );
            $this->sErrorStatus = self::STATUS_FORBIDDEN;
        }

        return $iElementId;
    }

    public function getStatus(): string
    {
        return $this->sErrorStatus;
    }

    public function getErrors(): array
    {
        return $this->arErrors;
    }

    public function getErrorDescription(): string
    {
        return implode(',', $this->getErrors());
    }

    public function isSuccess(): bool
    {
        return count($this->arErrors) === 0;
    }

    public static function chatIt($mxVar, $sName = 'Debug message', $sDialogId = '')
    {
        if (Option::get(self::MODULE_ID, 'debug') === 'Y') {
            try {
                if (!Loader::includeModule('im')) {
                    return;
                }
            } catch (LoaderException $e) {
                return;
            }

            if (empty($sDialogId)) {
                $sDialogId = trim(Option::get(self::MODULE_ID, 'debug_chat_id'));
            }

            if (!empty($sDialogId)) {
                $arFields = [
                    'MESSAGE_TYPE' => IM_MESSAGE_CHAT,
                    'DIALOG_ID'    => $sDialogId,
                    'MESSAGE'      => '[b]' . $sName . '[/b][br]' . print_r($mxVar, true),
                    'AUTHOR_ID'    => 1,
                    'FROM_USER_ID' => 1
                ];

                CIMMessenger::Add($arFields);
            }
        }
    }

    public function throwIfNeeded()
    {
        if (!$this->isSuccess()) {
            throw new RestException(implode(', ', $this->getErrors()), $this->getStatus(), (int) $this->getStatus());
        }
    }

}