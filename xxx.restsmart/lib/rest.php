<?php
/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 11.05.2023
 */

namespace Xxx\RestSmart;

use Xxx\RestSmart\General\Instruments;

class Rest
{

    public static function onRestServiceBuildDescription(): array
    {
        return [
            Instruments::MODULE_ID => [
                'xxx.smart.add' => [__CLASS__, 'addSmart'],
                'xxx.smart.update' => [__CLASS__, 'updateSmart']
            ]
        ];
    }

    public static function addSmart($arRequestData = [], $id = '', $obRestServer = null): array
    {
        $arResult = [
            'success' => true
        ];

        $obInstruments = new Instruments();
        if ($obInstruments->checkScope($obRestServer)) {
            $arFields = $obInstruments->prepareFields($arRequestData);
            if (!empty($arFields) && $obInstruments->isSuccess()) {
                $iSmartId = $obInstruments->update($arFields, true);
                if ($obInstruments->isSuccess() && $iSmartId > 0) {
                    $arResult['smart_id'] = $iSmartId;
                } else {
                    $arResult['success'] = false;
                    $arResult['errors'] = $obInstruments->getErrors();
                }
            } else {
                $arResult['success'] = $obInstruments->isSuccess();
                $arResult['errors'] = $obInstruments->getErrors();
            }
        } else {
            $arResult['success'] = false;
        }

        Instruments::chatIt(
            [
                '$arRequestData' => $arRequestData,
                '$arResult' => $arResult
            ],
            'xxx.smart.add'
        );

        $obInstruments->throwIfNeeded();

        return $arResult;
    }

    public static function updateSmart($arRequestData = [], $id = '', $obRestServer = null): array
    {
        $arResult = [
            'success' => true
        ];

        $obInstruments = new Instruments();
        if ($obInstruments->checkScope($obRestServer)) {
            $arFields = $obInstruments->prepareFields($arRequestData);
            if (!empty($arFields) && $obInstruments->isSuccess()) {
                $iSmartId = $obInstruments->update($arFields);
                if ($obInstruments->isSuccess() && $iSmartId > 0) {
                    $arResult['smart_id'] = $iSmartId;
                } else {
                    $arResult['success'] = false;
                    $arResult['errors'] = $obInstruments->getErrors();
                }
            }
        } else {
            $arResult['success'] = false;
        }

        Instruments::chatIt(
            [
                '$arRequestData' => $arRequestData,
                '$arResult' => $arResult
            ],
            'xxx.smart.update'
        );

        $obInstruments->throwIfNeeded();

        return $arResult;
    }

}